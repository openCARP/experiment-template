#!/usr/bin/env python

"""
This header should describe your experiment. You can use `restructured text (reST)
syntax <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`__.

See `the web page <https://opencarp.org/documentation/examples/02_ep_tissue/22_simple>`__ 
for a simple but complete example of a run.py script.

.. code-block:: bash

    ./run.py --some-parameter 100 --visualize
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'please add a short name'
EXAMPLE_AUTHOR = 'Your Name <your@mail.address>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

# import required carputils modules
from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

# define parameters exposed to the user on the commandline
def parser():
    parser = tools.standard_parser()
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_simple_{}_{}_np{}'.format(today.isoformat(), args.tend,
                                         args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    cmd = ""    # to be composed in your script

    # Run simulation
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        #geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        
        # display trensmembrane voltage
        #data = os.path.join(job.ID, 'vm.igb.gz')
        
        # load predefined visualization settings
        # view = os.path.join(EXAMPLE_DIR, 'simple.mshz')

        # Call meshalyzer
        job.meshalyzer(geom, data, view)

if __name__ == '__main__':
    run()
